#!/usr/bin/env bash
echo "Begin Build"
cd /tmp
pwd
wget https://dl.google.com/android/android-sdk_r24.4.1-linux.tgz
tar zxf android-sdk_r24.4.1-linux.tgz
rm android-sdk_r24.4.1-linux.tgz
export ANDROID_HOME="/tmp/android-sdk-linux"
export PATH="$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$PATH"

#( sleep 1 && while [ 1 ]; do sleep 1; echo y; done ) | android update sdk --no-ui --all --filter tools,platform-tools,build-tools-23.0.3,android-23,139,132,131

echo y | android update sdk --no-ui --all --filter tools
echo y | android update sdk --no-ui --all --filter platform-tools
echo y | android update sdk --no-ui --all --filter build-tools-23.0.3
echo y | android update sdk --no-ui --all --filter android-23
echo y | android update sdk --no-ui --all --filter 139
echo y | android update sdk --no-ui --all --filter 132
echo y | android update sdk --no-ui --all --filter 131

cd -
touch local.properties
echo "sdk.dir=$ANDROID_HOME" >> local.properties
chmod +x ./gradlew
